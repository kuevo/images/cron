# syntax=docker/dockerfile:1.4
FROM bitnami/minideb:latest

# Run as root
USER root

# Non interactive
ENV DEBIAN_FRONTEND=noninteractive

# Install the various packages
ARG PHP_VERSION="8.2"
ENV COMPOSER_ALLOW_SUPERUSER="1"
ENV COMPOSER_MEMORY_LIMIT="-1"
RUN \
    apt update && \
    apt upgrade -y && \
    install_packages \
        software-properties-common \
        ca-certificates \
        gpg \
        curl \
        gettext-base \
        cron \
    && \
    curl -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg && \
    sh -c 'echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list' && \
    apt update && \
    install_packages \
        php${PHP_VERSION} \
        php${PHP_VERSION}-cli \
        php${PHP_VERSION}-readline \
        php${PHP_VERSION}-common \
        php${PHP_VERSION}-mbstring \
        php${PHP_VERSION}-igbinary \
        php${PHP_VERSION}-apcu \
        php${PHP_VERSION}-imagick \
        php${PHP_VERSION}-yaml \
        php${PHP_VERSION}-bcmath \
        php${PHP_VERSION}-mysql \
        php${PHP_VERSION}-mysqlnd \
        php${PHP_VERSION}-mysqli \
        php${PHP_VERSION}-zip \
        php${PHP_VERSION}-bz2 \
        php${PHP_VERSION}-gd \
        php${PHP_VERSION}-msgpack \
        php${PHP_VERSION}-intl \
        php${PHP_VERSION}-zstd \
        php${PHP_VERSION}-redis \
        php${PHP_VERSION}-curl \
        php${PHP_VERSION}-opcache \
        php${PHP_VERSION}-xml \
        php${PHP_VERSION}-soap \
        php${PHP_VERSION}-exif \
        php${PHP_VERSION}-xsl \
        php${PHP_VERSION}-gettext \
        php${PHP_VERSION}-cgi \
        php${PHP_VERSION}-dom \
        php${PHP_VERSION}-ftp \
        php${PHP_VERSION}-iconv \
        php${PHP_VERSION}-pdo \
        php${PHP_VERSION}-simplexml \
        php${PHP_VERSION}-tokenizer \
        php${PHP_VERSION}-xml \
        php${PHP_VERSION}-xmlwriter \
        php${PHP_VERSION}-xmlreader \
        php${PHP_VERSION}-fileinfo \
    && \
    # Shrink binaries
    (find /usr/local/bin -type f -print0 | xargs -n1 -0 strip --strip-all -p 2>/dev/null || true) && \
    (find /usr/local/lib -type f -print0 | xargs -n1 -0 strip --strip-all -p 2>/dev/null || true) && \
    # Cleanup
    rm -rf /tmp/* /src

# Copy in the module configurations
ARG PHP_VERSION="8.2"
COPY --link config/modules/* /etc/php/${PHP_VERSION}/mods-available/

# Copy in the placeholders
COPY --link config/* /configs/

# Copy in the entrypoint and make it executable
COPY --link entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Set workdir
WORKDIR /app

ARG PHP_VERSION="8.2"
ENV PHP_VERSION=${PHP_VERSION}
ARG PHP_MEMORY_LIMIT="-1"
ENV PHP_MEMORY_LIMIT=${PHP_MEMORY_LIMIT}
ARG PROJECTTYPE="magento"
ENV PROJECTTYPE=${PROJECTTYPE}

WORKDIR /app
ENTRYPOINT ["/entrypoint.sh"]
