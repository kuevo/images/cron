#!/bin/sh

set -e

# Set ENV vars for php.ini
envsubst "$(env | sed -e 's/=.*//' -e 's/^/$/g')" < /configs/php.ini.tmpl > /etc/php/$PHP_VERSION/cli/php.ini

# Transfer the env variables into the cron environment
env >> /etc/environment

crontab -u root /configs/crontab-$PROJECTTYPE
cron -f

#-s            directory of system crontabs (defaults to /etc/cron.d)
#-c            directory of per-user crontabs (defaults to /etc/crontabs)
#-t            directory of timestamps (defaults to /var/spool/cron/cronstamps)
#-m user@host  where should cron output be directed? (defaults to local user)
#-M mailer     (defaults to /usr/sbin/sendmail)
#-S            log to syslog using identity 'crond' (default)
#-L file       log to specified file instead of syslog
#-l loglevel   log events <= this level (defaults to notice (level 5))
#-b            run in background (default)
#-f            run in foreground
#-d            run in debugging mode
